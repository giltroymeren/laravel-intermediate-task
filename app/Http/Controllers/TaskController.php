<?php

namespace App\Http\Controllers;

use App\Task;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\TaskRepository;

class TaskController extends Controller
{
    /**
     * The task repository instance.
     *
     * @var TaskRepository
     */
    protected $tasks;

    /**
     * Create a new controller instance.
     *
     * @param TaskRepository $tasks
     * @return void
     */
    public function __construct(TaskRepository $tasks)
    {
        $this->middleware('auth');

        $this->tasks = $tasks;
    }

    /**
     * Gets a single task.
     *
     * @param  Request  $request
     * @return Response
     */
    public function get(Request $request, $id)
    {
        return view('tasks.index', [
            'tasks' => $this->tasks->getAllByUser($request->user()),
            'task' => $this->tasks->getByUser($request->user(), $id),
            'action' => 'Update',
            'form_action' => '/tasks/update'
        ]);
    }

    /**
     * Display a list of all of the user's task.
     *
     * @param  Request  $request
     * @return Response
     */
    public function getAll(Request $request)
    {
        return view('tasks.index', [
            'tasks' => $this->tasks->getAllByUser($request->user()),
            'action' => 'Create',
            'form_action' => '/tasks/create'
        ]);
    }

    /**
     * Create a new task.
     *
     * @param  Request  $request
     * @return Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'deadline_at' => 'required|date_format:Y-m-d'
        ]);

        $request->user()->tasks()->create([
            'name' => $request->name,
            'deadline_at' => $request->deadline_at,
        ]);

        return redirect('/tasks');
    }

    /**
     * Update a given task.
     *
     * @param Request  $request
     * @return Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
        ]);

        $this->tasks->getByUser($request->user(), $request->id)[0]
            ->update([
            'name' => $request->name,
            'deadline_at' => $request->deadline_at,
        ]);

        return redirect('/tasks');
    }

    /**
     * Destroy the given task.
     *
     * @param  Request  $request
     * @param  Task  $task
     * @return Response
     */
    public function delete(Request $request, Task $task)
    {
        $this->authorize('delete', $task);

        $task->delete();

        return redirect('/tasks');
    }
}