<?php

namespace App\Repositories;

use App\User;

class TaskRepository
{
    /**
     * Get all of the tasks for a given user.
     *
     * @param  User  $user
     * @return Collection
     */
    public function getAllByUser(User $user)
    {
        return $user->tasks()
            ->orderBy('created_at', 'asc')
            ->get();
    }

    /**
     * Get a task for a given user.
     *
     * @param  User  $user
     * @param  Integer  $id
     * @return Collection
     */
    public function getByUser(User $user, $id)
    {
        return $user->tasks()
            ->where('id', $id)
            ->get();
    }
}