[Laravel 5.2 Quickstart Intermediate](https://laravel.com/docs/5.2/quickstart-intermediate)

if fresh checkout/clone, perform the following commands:
-----
- `composer install`
> "Whoops, looks like something went wrong."
- check if `/.env` exists. If not, then use `/.env.example` and make necessary changes as needed.
> RuntimeException in Encrypter.php line 43:
> The only supported ciphers are AES-128-CBC and AES-256-CBC with the correct key lengths.
- `php artisan key:generate`


Reference: ["Whoops, looks like something went wrong. Laravel 5.0"](http://stackoverflow.com/a/31518979)