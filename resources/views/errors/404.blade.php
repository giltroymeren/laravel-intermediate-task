<!-- resources/views/errors/404.blade.php -->

@extends('layouts.app')

@section('content')

<div class="panel panel-danger">
    <div class="panel-heading">
        <h2 class="text-danger">Page Not Found</h2>
    </div>
    <div class="panel-body">
        <p><a href="{{ url('/home') }}">Go Back Home</a></p>
    </div>
</div>

@endsection