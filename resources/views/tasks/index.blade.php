<!-- resources/views/tasks/index.blade.php -->

@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            {{ $action }} Task
        </div>
        <div class="panel-body">
            <form action="{{ $form_action }}" method="POST" class="form-horizontal">
                {{ csrf_field() }}

                @if (isset($task))
                <input type="hidden" name="id" id="update-task-id" class="form-control"
                    value="{{ $task[0]->id }}">
                @endif

                <div class="form-group">
                    <label for="task" class="col-sm-2 control-label">Task</label>

                    <div class="col-sm-10">
                        <input type="text" name="name" id="create-task-name"
                            class="form-control" value="{{ (isset($task)) ? $task[0]->name : old('name') }}">

                        @if ($errors->has('name'))
                        <div class="alert alert-danger" role="alert">
                            {{ $errors->first('name') }}
                        </div>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label for="deadline_at" class="col-sm-2 control-label">Deadline</label>

                    <div class="col-sm-10">
                        <div class="input-group date" data-provide="datepicker">
                            <input type="text" class="form-control" name="deadline_at"
                                value="{{ (isset($task)) ? $task[0]->deadline_at : old('deadline_at') }}">
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-th"></span>
                            </div>
                        </div>
                        @if ($errors->has('deadline_at'))
                        <div class="alert alert-danger" role="alert">
                            {{ $errors->first('deadline_at') }}
                        </div>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <a href="/tasks" role="button" class="btn btn-danger">
                            Cancel
                        </a>
                        <button type="submit" class="btn btn-primary">
                            {{ $action }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    @if (count($tasks) > 0)
        <div class="panel panel-default">
            <div class="panel-heading">
                Current Tasks
            </div>

            <table class="table table-hover task-table">

                <thead>
                    <th class="text-right">ID</th>
                    <th>Task</th>
                    <th class="text-right">Deadline</th>
                    <th class="text-right">Created</th>
                    <th class="text-right">Updated</th>
                    <th>Update</th>
                    <th>Delete</th>
                </thead>

                <tbody>
                    @foreach ($tasks as $task)
                        <tr class="{{ ($task->created_at != $task->updated_at) ? 'warning' : '' }}">
                            <td class="table-text text-right">
                                <div><code>{{ $task->id }}</code></div>
                            </td>

                            <td class="table-text">
                                <div>{{ $task->name }}</div>
                            </td>

                            <td class="table-text text-right">
                                <div><code>{{ $task->deadline_at }}</code></div>
                            </td>

                            <td class="table-text text-right">
                                <div><code>{{ $task->created_at->format('Y-m-d') }}</code></div>
                            </td>

                            <td class="table-text text-right">
                                <div><code>{{ $task->updated_at->format('Y-m-d') }}</code></div>
                            </td>

                            <td>
                                <form action="/tasks/update/{{ $task->id }}" method="GET">
                                    {{ csrf_field() }}
                                    {{ method_field('UPDATE') }}

                                    <button class="btn btn-primary btn-xs">
                                        <span class="glyphicon glyphicon-pencil"
                                            aria-hidden="true"></span>
                                    </button>
                                </form>
                            </td>

                            <td>
                                <form action="/tasks/delete/{{ $task->id }}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}

                                    <button class="btn btn-danger btn-xs"
                                        onclick="return confirmTaskAction('Delete');">
                                        <span class="glyphicon glyphicon-remove"
                                            aria-hidden="true"></span>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <script type="text/javascript">
            function confirmTaskAction(action) {
                return (confirm(action + ' this Task?')) ? true : false;
            }
        </script>
    @endif

    <script type="text/javascript">
        $.fn.datepicker.defaults.format = "yyyy-mm-dd";
        $('.datepicker').datepicker();
    </script>
@endsection