<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/{url}', function () {
    return view('home');
})->where('url', '(home)?');

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::auth();

Route::get('/tasks', 'TaskController@getAll');
Route::get('/tasks/{url}', 'TaskController@getAll')
    ->where('url', '(create)?');

Route::post('/tasks/create', 'TaskController@create');

Route::get('/tasks/update/{id}', 'TaskController@get');

Route::post('/tasks/update', 'TaskController@update');

Route::delete('/tasks/delete/{task}', 'TaskController@delete');
